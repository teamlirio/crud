package com.normz.controller;
import java.sql.PreparedStatement;

import java.sql.SQLException;

import com.normz.connection.CrudConnection;
public class DeleteUserProcess {
	private CrudConnection cc = new CrudConnection();
	public String deleteUser(String id){
		
		try {
			String query = "Delete from crud where id = ?";
			
			PreparedStatement pstate = cc.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			int row = pstate.executeUpdate();
			if(row > 0){
				return "Delete Successful!";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "Failed to delete user. Please try again.";
	}
	
	
}
