package com.normz.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.normz.model.User;

@WebServlet("/registeruser")
public class RegisterUser extends HttpServlet {
	private User c = new User();
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			String fname = request.getParameter("fname");
			String lname = request.getParameter("lname");
			String address = request.getParameter("address");
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			String confirmPassword = request.getParameter("confirmPassword");
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			Date dob = format.parse(request.getParameter("dob"));
			
			c = new User(fname, lname, address, password,username,dob, confirmPassword);
			RegisterUserProcess rup = new RegisterUserProcess();
			c = rup.registerUser(c);
			out.println(c.getAttempt());
			
			out.println("<a href='registration.jsp'>Registration page</a>");
			
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
