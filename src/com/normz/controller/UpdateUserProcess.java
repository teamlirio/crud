package com.normz.controller;

import com.normz.connection.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.normz.model.*;

public class UpdateUserProcess {
	private CrudConnection cc = new CrudConnection();
	public User updateUser(User user){
		try {
			
			String query = "UPDATE crud SET firstname=?, lastname=?, address=?, DoB=?, username=? where id=?";
			PreparedStatement pstate = cc.getConnection().prepareStatement(query);
			pstate.setString(1, user.getFname());
			pstate.setString(2, user.getLname());
			pstate.setString(3, user.getAddress());
			java.sql.Date sqlDate = new java.sql.Date(user.getDate().getTime());
			pstate.setDate(4, sqlDate);
			pstate.setString(5, user.getUsername());
			pstate.setString(6, user.getId());
			
			if(!pstate.execute()) {
				return new User("Update Saved!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new User("Failed to update user. Please try again.<br>");
	}
	
	public ResultSet viewInfo(String id){
		ResultSet rs = null;
		
		try {
			String query = "Select * from crud where id = ?";
			PreparedStatement state = cc.getConnection().prepareStatement(query);
			state.setString(1, id);
			rs = state.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}
}
